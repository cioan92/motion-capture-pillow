%% MCP Classifier
% The program reads fetures from the MCP application 
% feature vector: [rob_x,rob_y,rob_z,rot_x,rot_y,rot_z,[pinNo,..... ] for pinNo ? [1,171]]
% and uses an 
clear all
close all

%% 1. Extract features from folder 
iter = 1;

% Read all .csv feature files 
files =  dir('TestData/Test_4/*.csv');
for file = files'
    
    filenm = [file.folder '\' file.name];
    % Read the state of the robot
    robot_state = csvread(filenm,1,0,[1,0,1,5]);
    
    % Read the pin features
    pin_states = csvread(filenm,3,0);
    
    % keep only those to be used by the classifier
    pin_states = pin_states(:,[5:6,9:12])';
    
    % construct feature vector dataset
    feature_vector(:,iter) =  [robot_state pin_states(:)']';

    iter = iter + 1;
end
disp('Feature extraction end')

%% 2.  Split set (40% training, 10% validation, 50% test ) - not to be used
% in final test
% [train_1, val_1, test_1] = dividerand(feature_vector,0.4,0.1,0.5);

fvSize = size(feature_vector);
train_1 = feature_vector(:,1:1:fvSize(2));
test_1 = feature_vector(:,2:2:fvSize(2));

train_1 = train_1';
% val_1 = val_1';
test_1 = test_1';

%% 3. Feedforward neural networks

% % disp('Press a button to proceed')
% pause;

inputs = train_1(:,7:1032)'; % input vector (6-dimensional pattern)
outputs = train_1(:,1:3)'; % corresponding target output vector

% create network
net = network( ...
1, ... % numInputs, number of inputs,
2, ... % numLayers, number of layers
[1; 0], ... % biasConnect, numLayers-by-1 Boolean vector,
[1; 0], ... % inputConnect, numLayers-by-numInputs Boolean matrix,
[0 0; 1 0], ... % layerConnect, numLayers-by-numLayers Boolean matrix
[0 1] ... % outputConnect, 1-by-numLayers Boolean vector
);
% View network structure
view(net);

% number of hidden layer neurons
net.layers{1}.size = 4;
% hidden layer transfer function
net.layers{1}.transferFcn = 'logsig';
view(net);

net = configure(net,inputs,outputs);
view(net);

% initial network response without training
initial_output = net(inputs);
% network training
net.trainFcn = 'trainlm';
net.performFcn = 'mse';
net = train(net,inputs,outputs);
disp('Network trained')

% network response after training
outputs = net(inputs)
disp('Outputs Generated')

%% 5. Plots
% A. plot difference between actual and predicted values
diff = abs(outputs(:,:) - train_1(:,1:3)')

subplot(3,2,1);
plot(diff(1,:))
hold on 
axis equal
ylabel('dx','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,3);
plot(diff(2,:))
axis equal
ylabel('dy','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,5);
plot(diff(3,:))
axis equal
ylabel('dz','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 

subplot(3,2,2);
plot(test_1(:,1))
axis equal
hold on 
ylabel('X','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,4);
plot(test_1(:,2))
axis equal
ylabel('Y','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,6);
plot(test_1(:,3))
axis equal
ylabel('Z','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 

disp('End')

