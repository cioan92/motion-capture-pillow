close all
clear all

%% 1. Extract features from folder 
iter = 1;

% Read all .csv feature files 
% files = dir('*.csv');
files =  dir('TestData/Test_5/*.csv');

figure(1)
marker = 'x';
hold on
for file = files'
    
    filenm = [file.folder '\' file.name]
    % Read the state of the robot
    robot_state = csvread(filenm,1,0,[1,0,1,5]);
    
    % Read the pin features
    pin_states = csvread(filenm,3,0);
    plot(pin_states(:,5),pin_states(:,6),marker)
    
    iter = iter + 1;
    marker = '.';
    pause(.01)
end
disp('Feature extraction end')
disp('Total frames processed:')
iter


