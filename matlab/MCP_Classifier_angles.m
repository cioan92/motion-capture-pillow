%% MCP Classifier
% The program reads fetures from the MCP application 
% feature vector: [rob_x,rob_y,rob_z,rot_x,rot_y,rot_z,[pinNo,..... ] for pinNo ? [1,171]]
% and uses an 
clear all
close all

%% 1. Extract features from folder 
iter = 1;

% Read all .csv feature files 
files =  dir('TestData/Test_4/*.csv');
for file = files'
    
    filenm = [file.folder '\' file.name];
    % Read the state of the robot
    robot_state = csvread(filenm,1,0,[1,0,1,5]);
    
    % Read the pin features
    pin_states = csvread(filenm,3,0);
    
    % keep only those to be used by the classifier [currX,currY, dx, dy, distance, bearing]
    pin_states = pin_states(:,[5:6,9:12])';
    
    % construct feature vector dataset
    feature_vector(:,iter) =  [robot_state pin_states(:)']';

    iter = iter + 1;
end
disp('Feature extraction end')

%% 2.  Split set (40% training, 10% validation, 50% test ) - not to be used
% in final test
% [train_1, val_1, test_1] = dividerand(feature_vector,0.4,0.1,0.5);

fvSize = size(feature_vector);
train_1 = feature_vector(:,1:2:fvSize(2));
test_1 = feature_vector(:,2:2:fvSize(2));

train_1 = train_1';
% val_1 = val_1';
test_1 = test_1';

%% 3. Radial Basis Functions
goal = 0;
spread = 1;
% choose max number of neurons
K = 500;
% number of neurons to add between displays
Ki = 50;
% RBF train [theta 1] (1: inputs, 2: outputs, 3: performance goal)
rbfNetwork = newrb(train_1(:,7:1032)',train_1(:,1:6)', goal, spread, K, Ki);

%view network structure:
view(rbfNetwork)

%% 4.  Validating RBF network
outputs = sim(rbfNetwork, test_1(:,7:1032)');

%% 5. Plots

% A. plot difference between actual and predicted values
% X,Y,Z position
diff = abs(outputs(:,:) - test_1(:,1:6)')

figure(1)
subplot(3,2,1);
plot(diff(1,:))
hold on 
ylabel('dx','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,3);
plot(diff(2,:))
ylabel('dy','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,5);
plot(diff(3,:))
ylabel('dz','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 

subplot(3,2,2);
plot(test_1(:,1))
hold on 
ylabel('X','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,4);
plot(test_1(:,2))
ylabel('Y','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,6);
plot(test_1(:,3))
ylabel('Z','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 

figure(2);
plot3(test_1(:,1),test_1(:,2),test_1(:,3),'o')

% Euler angles rx,ry,rz

figure(3)
subplot(3,2,1);
plot(diff(4,:))
hold on 
ylabel('drx','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,3);
plot(diff(5,:))
ylabel('dry','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,5);
plot(diff(6,:))
ylabel('drz','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 

subplot(3,2,2);
plot(test_1(:,4))
hold on 
ylabel('rX','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,4);
plot(test_1(:,5))
ylabel('rY','fontsize',10)
% title('bbb','fontsize',10)

subplot(3,2,6);
plot(test_1(:,6))
ylabel('rZ','fontsize',10)
xlabel('frame','fontsize',10)
% title('bbb','fontsize',10)
hold on 





