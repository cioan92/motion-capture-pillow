import cv2
from operator import itemgetter
import math
import itertools
import numpy as np

__all__ = ['chunker', 'rw', 'Pillow', 'Pins']

def chunker(seq, size):
    return [seq[pos:pos + size] for pos in xrange(0, len(seq), size)]

'''
class rw()
    Used to read and export the features to .txt files
'''
class rw():
    def readFile2List(self, textFile):
        with open(textFile, "r") as file:
            data = []
            for line in file.readlines():
                data.append([float(i) for i in line.split()])
        return data

    def writeList2File(self, textFile, DATA):
        with open(textFile, "w") as file:
            DATA = '\n'.join('\t'.join(map(str,j)) for j in DATA)
            file.write(DATA)

'''
    class Pillow:
    accepts a camera frame that looks into the pillow pins
    implementation of image processing functions for cropping,
    filtering the image, extract features from frame
'''
class Pillow:

    def __init__(self, frame, refPt):
        self.origFrame = frame
        self.refPt = refPt
        self.modFrame = frame
        self.x1 = 0
        self.y1 = 0
        self.x2 = 0
        self.y2 = 0

    def cropFrame(self):
        """ Crops the original image
            Returns:: the original image with the cropped region highlighted
        """

        self.x1,self.y1 = self.refPt[0][0],self.refPt[0][1]
        self.x2,self.y2 = self.refPt[1][0],self.refPt[1][1]
        # Crop to ROI (Region of Interest)
        self.modFrame = self.origFrame[self.y1:self.y2, self.x1:self.x2]
        frame_with_box = cv2.rectangle(self.origFrame, (self.x1,self.y1), (self.x2,self.y2), (0,255,0), 1)

        return frame_with_box

    def getFrameBaysian(self, kernel):
        """ Apply a high pass filter on the image
            Inputs:: [kernel] should be an odd number (used to create a kxk kernel)
        """

        image = cv2.cvtColor(self.modFrame, cv2.COLOR_BGR2GRAY)             # a.  Convert the frame to GRAY
        blur = cv2.GaussianBlur(image,(kernel,kernel),0)                    # b. Apply GaussianBlur (Low pass filtering)
        filtered = image - blur                                             # c. Subtract to get the higher frequencies
        self.modFrame = filtered + 50*np.ones((filtered.shape), np.uint8)   # d. Multiply with a constant (Brighten/Darken)

        return self.modFrame

    def applyThreshold(self, threshold = 0, adaptive = True ):
        """ Apply a threshold on the image
        """
        if adaptive == True:
            print("here")
            frame = cv2.threshold(self.modFrame,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]
        else:
            frame = cv2.threshold(self.modFrame, threshold, 255, cv2.THRESH_BINARY)[1]

        self.modFrame = frame
        return self.modFrame

    def applyGaussianBlur(self, kernel):
        self.modFrame = cv2.GaussianBlur(self.modFrame,(kernel,kernel),0)
        return self.modFrame

    def rotateImage(self, degrees):
        ROI = self.modFrame
        rows,cols = ROI.shape
        M = cv2.getRotationMatrix2D((cols/2,rows/2),degrees,1)
        self.modFrame = cv2.warpAffine(self.modFrame,M,(cols,rows))

        return self.modFrame

    def getFrame(self):
        """ Apply Transformations to the image,
            - conversion to greyscale
            - apply a simple threshold
            -
        Deprecated, used in Paul's initial research"""

        frame_with_box = self.cropFrame()

        # Convert the frame to GRAY, and blur it
        image = cv2.cvtColor(self.modFrame, cv2.COLOR_BGR2GRAY)
        thresholded = cv2.threshold(image, 200, 255, cv2.THRESH_BINARY)[1]

        # Dialate the thresholded image to fill in holes
        image = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, None, iterations = 2)
        self.modFrame = cv2.dilate(image, None, iterations = 3)

        return self.modFrame, frame_with_box

    def getWatershedTransform(self):
            """ Segment images using the Watershed transform
                Deprecated: Not used in the final algorithm
            """

            # Watershed transformation
            img = cv2.cvtColor(self.modFrame,cv2.COLOR_GRAY2RGB)
            gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            cv2.imshow("Image",img)

            ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
            cv2.imshow("ThresholdedImage",thresh)
            # noise removal
            kernel = np.ones((3,3),np.uint8)
            opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)
            # sure background area
            sure_bg = cv2.dilate(opening,kernel,iterations=3)
            # Finding sure foreground area
            dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
            ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
            cv2.imshow("SureImage",sure_fg)
            # Finding unknown region
            sure_fg = np.uint8(sure_fg)
            unknown = cv2.subtract(sure_bg,sure_fg)

            # Marker labelling
            ret, markers = cv2.connectedComponents(sure_fg)
            # Add one to all labels so that sure background is not 0, but 1
            markers = markers+1
            # Now, mark the region of unknown with zero
            markers[unknown==255] = 0
            markers = cv2.watershed(img,markers)
            img[markers == -1] = [255,0,0]

            self.modFrame = img
            return self.modFrame

    def resetFrame(self):
        self.modFrame = origFrame

    def getModifiedFrame(self):
        return self.modFrame

    def setModifiedFrame(self, frame):
        self.modFrame = frame

    def detectorParameters(self):
        """ Set up the blob detector parameters
            Returns a SimpleBlobDetector object
        """
        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()
        # # Change thresholds
        params.minThreshold = 10
        params.maxThreshold = 255
        # # Filter by Colour - (for some reason if I remove this it crashes)
        params.filterByColor = False
        params.blobColor = 255
        # # Filter by Circularity
        params.filterByCircularity = True
        params.minCircularity = 0.7
        # # Filter by Convexity
        # params.filterByConvexity = True
        # params.minConvexity = 0.87
        # # Filter by Area.
        params.filterByArea = True
        params.minArea = 60
        # Create a detector with the parameters
        ver = (cv2.__version__).split('.')
        if int(ver[0]) < 4:
            self.detector = cv2.SimpleBlobDetector_create(params)
        else :
            self.detector = cv2.SimpleBlobDetector_create(params)
        return self.detector


    def measurements(self,data1,data2,L2):
        """ Get the measurements
            data1 refers to the pin original positions and data2 to the current
            pin position. The function extracts the following measurements:
            - dx,dy: displacement on the x and y axis respectively
            - bearing: angle of motion for the pin (designates direction)
            - state: (0.0 or 1.0) depending on whether the displacement is big or small (used in drawing)
            - distance: Eucleidian distance btw reference and current position
            """
        self.DistanceBearing = []

        for i in xrange(L2):
            # Calculate the distance traveled
            distance = round(math.sqrt((round(data2[i][1], 1) - round(data1[i][1], 1))**2 + (round(data2[i][2], 1) - round(data1[i][2], 1))**2), 1)
            changeinSize = round(data2[i][3] - data1[i][3],2)
            # Calculate the bearing
            dy = (data2[i][2] - data1[i][2])
            dx = (data2[i][1] - data1[i][1])

            if dx > 0:
                if dy > 0:
                    bearing = math.atan((dx/dy))
                elif dy < 0:
                    bearing = math.pi + math.atan((dx/dy))
                else:
                    bearing = (math.pi)/2
            elif dx < 0:
                if dy > 0:
                    bearing = 2*(math.pi) + math.atan((dx/dy))
                elif dy < 0:
                    bearing = math.pi + math.atan((dx/dy))
                else:
                    bearing = (3/2)*math.pi
            else:
                if dy > 0:
                    bearing = 0
                elif dy < 0:
                    bearing = math.pi
                else:
                    bearing = 0

            if (distance < 1 or distance > 25):
                state = 0.0
            else:
                state = 1.0

            bearing = round(bearing*(180/math.pi),1)
            distance = round(math.pow(distance,2),1)
            self.DistanceBearing.append([data1[i][0], state, dx, dy, distance, bearing,changeinSize])
            self.DistanceBearing.sort(key = itemgetter(0),reverse = False)

            if len(self.DistanceBearing) == L2:
                # from Pillow import rw
                rwc = rw()
                rwc.writeList2File("Pin_Distances_Bearings.txt", self.DistanceBearing)

        return self.DistanceBearing


    def blobCheck(self, coords, xyn, rows, cols):
        """ Get the blob name from a predefined blobposition"""
        for i in xrange(len(xyn)):
            if coords[1] < xyn[i][1]:
                for j in xrange(cols):
                    if coords[0] < xyn[j][0]:
                        return xyn[i + j][2]


    def getDataSet(self, keypoints, xyn, cols, rows):
        """ Setup the frame for blob detection
            Get the Pin positions
            returns: data = [pin_no, origX, origY, origSize]
        """

        # Set the detectors parametors and detect blobs.
        coords1 = [x.pt + (x.size,) for x in keypoints]
        data = []

        for coordinates in coords1:
            coords = [round(coordinates[0],1), round(coordinates[1],1)]
            blobNum1 = self.blobCheck(coords, xyn, cols, rows)
            data.append([int(blobNum1),round(coordinates[0],1), round(coordinates[1],1),round(coordinates[2],3)])

        data.sort(key=itemgetter(0), reverse = False)
        return data

    def getDataSetLK(self, pins, ref = False):
        """
            Get the Pin positions (derived from L&K)
            imputs pins numpy array of numpy arrays [[locx,locy],...]
            returns: data = [pin_no, origX, origY, 0]
        """
        data = []

        for pinNo in range(pins.size/2):
            dtPt = [ pinNo + 1, round(pins[pinNo][0][0],1), round(pins[pinNo][0][1],1), 0 ]
            data.append(dtPt)

        if ref :
            rwc = rw()
            rwc.writeList2File("Ref_pin_positions_LK.txt", data)

        return data

    def colorMap(self, DistanceBearing):
        """map the blob data with a certain colour"""
        colorMap = []

        for i in xrange(len(DistanceBearing)):
            Colour = int(100 + math.pow((DistanceBearing[i][2]), 2))

            if (DistanceBearing[i][2] < 10):
                color = (Colour, 0, 0)
            elif DistanceBearing[i][2] < 15:
                color = (Colour, Colour, 0)
            elif DistanceBearing[i][2] < 20:
                color = (0, Colour, 0)
            elif DistanceBearing[i][2] < 30:
                color = (0, Colour, Colour)
            elif DistanceBearing[i][2] < 40:
                color = (0, 0, Colour)

            colorMap.append(color)
        return colorMap

'''
class Pins():
    Classifies the pins inside the image
'''
class Pins():

    def __init__(self,refPt):
        self.x1, self.y1 = refPt[0][0],refPt[0][1]
        self.x2, self.y2 = refPt[1][0],refPt[1][1]

    def countRnCDeprecated(self,data):
        Columns = 1
        Rows = 1
        for i in xrange(len(data)-1):
            if ((data[i][1] + 15) > data[i+1][1]) & (data[i+1][1] > (data[i][1] - 15)):
                Columns = Columns + 1
            else:
                Rows = Rows + 1
                Columns = 1
        return Columns, Rows

    def countRnC(self,data):
        ''' Counts the rows and columns of pins (blobs in the image), provided
            they are distributed as a matrix.
            The algorithm works under the assumption that blob detector scans
            from top to bottom (first gets the topmost rows, then continues)
        '''
        Columns = 1
        Rows = 1
        regionSize = 28

        maxCols = 0
        currDtPt = data[1]
        for i in xrange(len(data)-2):

            print("Currnt Data pt:", currDtPt, "Data point:" , data[i+1], "Diff in y:", abs(currDtPt[1] - data[i+1][1]))

            if (abs(currDtPt[1] - data[i+1][1]) > regionSize):
                currDtPt = data[i+1]
                Rows += 1

                if Columns > maxCols:
                    maxCols = Columns

                    print(" >>  maximumColumns:", maxCols )
                Columns = 1
                print(" >>  column reset:" )
            else:
                Columns +=1

        return maxCols, Rows

    def vertical(self,Rows,coords):
        coords = sorted(coords, key=itemgetter(0))
        b, minmax = [], []
        for a in xrange(0,len(coords),Rows):
            b.append(coords[a:a+Rows])
        for i in xrange(len(b)):
            c = []
            for j in xrange(len(b[i])):
                c.append(b[i][j][0])
            minmax.append([min(c),max(c)])

        minmax[len(minmax)-1][1] = self.x2-self.x1
        minmax.append([self.x2-self.x1,self.x2-self.x1])
        midpoints = [(round((minmax[i+1][0] - minmax[i][1]),2))/2 for i in xrange(len(minmax)-1)]

        return midpoints, minmax

    def horizontal(self,Columns,coords):
        b, minmax = [], []
        for a in xrange(0,len(coords),Columns):
            b.append(coords[a:a+Columns])
        for i in xrange(len(b)):
            c = []
            for j in xrange(len(b[i])):
                c.append(b[i][j][1])
            minmax.append([min(c),max(c)])

        minmax[len(minmax)-1][1] = self.y2-self.y1
        minmax.append([self.y2-self.y1,self.y2-self.y1])
        midpoints = [(round((minmax[i+1][0] - minmax[i][1]),2))/2 for i in xrange(len(minmax)-1)]

        return midpoints, minmax

    def main(self,keypoints):
        roundedCoordinates, crosspointsx, crosspointsy = [], [], []
        coordinates = [keypoints[i-1].pt for i in xrange(len(keypoints))]
        coordinates = sorted(coordinates, key=itemgetter(1))                    # sort by y coord - helps distinguish between rows

        [roundedCoordinates.append((round(coordinates[i][0],1),round(coordinates[i][1],1))) for i in xrange(len(coordinates))]

        # ok for some reason it does not return accureta cols and rows
        Columns, Rows = self.countRnCDeprecated(roundedCoordinates)

        blank_image = np.zeros((720,1280,3), np.uint8)
        for xx in roundedCoordinates:
            cv2.circle(blank_image,((int(xx[0]),int(xx[1])) ), 15, (0,100,100), -1)
            cv2.imshow("Pins",blank_image)
            #cv2.waitKey(100)

        # this should change
        Vmidpoints, Vminmax = self.vertical(Rows,roundedCoordinates)
        Hmidpoints, Hminmax = self.horizontal(Columns,roundedCoordinates)

        for i in xrange(len(Hmidpoints)):
            for j in xrange(len(Vmidpoints)):
                crosspointsx.append((int(Vmidpoints[j])+int(Vminmax[j][1])))
                crosspointsy.append((int(Hmidpoints[i])+int(Hminmax[i][1])))
        crosspoints = zip(crosspointsx,crosspointsy)

        for i in xrange(len(crosspoints)):
            crosspoints[i] = crosspoints[i]+(i+1,)

        for xx in crosspoints:
            cv2.circle(blank_image,((int(xx[0]),int(xx[1])) ), 10, (0,0,200), -1)
            cv2.imshow("Pins",blank_image)
            #cv2.waitKey(100)

        rwc = rw()
        rwc.writeList2File("Pin_Regions.txt",crosspoints)
        return Columns, Rows


    ''' def pinTracker(self,keypoints):
        Finds the pin locations, constructs a grid (cols, rows) and assigns
        each pin to its place on the grid
        returns the data with the assigned keypoints
    '''
    def pinTracker(self,keypoints):

        roundedCoordinates, datapts = [], []
        coordinates = [keypoints[i-1].pt for i in xrange(len(keypoints))]
        coordinates = sorted(coordinates, key=itemgetter(1))                     # sort by y coord - helps distinguish between rows

        [roundedCoordinates.append((round(coordinates[i][0],1),round(coordinates[i][1],1))) for i in xrange(len(coordinates))]

        # returns the rows and colums of the grid of pins, provided that
        # the pins are in fact arranged as a grid
        cols, rows = self.countRnC(roundedCoordinates)
        print(">> rows,cols:",rows,cols)

        cols = cols*2
        rows = rows*2

        # get top left and bottom right coordinates to establish grid boundaries
        maxX = max(roundedCoordinates, key=itemgetter(0))[0]
        maxY = max(roundedCoordinates, key=itemgetter(1))[1]
        minX = min(roundedCoordinates, key=itemgetter(0))[0]
        minY = min(roundedCoordinates, key=itemgetter(1))[1]

        # establish grid size
        gSize   = (abs(minX - maxX),abs(minY - maxY))
        gLength = (int(gSize[0]/cols) , int(gSize[1]/rows))

        for pItem in roundedCoordinates:
            rowLoc = int(abs(pItem[1] - minY)/(gLength[1]))
            colLoc = int(abs(pItem[0] - minX)/(gLength[0]))
            print(">> Item # row,col:",rowLoc,colLoc)

            pinNo = rowLoc*cols + colLoc        # get the location of the pin on the grid
            print(">> Item # assingned number:", pinNo)

            datapts.append([pItem, pinNo])

        datapts = sorted(datapts, key=itemgetter(1))
        print(datapts)
        print("---- Success? -----")
        print(roundedCoordinates)

        # rearrange points from 1-totalPinNo
        for i in xrange(len(datapts)):
            datapts[i][1] = i

        blank_image = np.zeros((720,1280,3), np.uint8)
        # draw grid
        # cv2.circle(blank_image, (int(maxX),int(maxY)), 5, (100,100,100), -1)
        # cv2.circle(blank_image, (int(minX),int(minY)), 5, (100,100,100), -1)
        #
        # lineThickness = 2
        #
        # for x in xrange(cols):
        #     x2 = int(minX) + gLength[0]*x
        #     cv2.line(blank_image, (x2, int(minY)), (x2, int(maxY)), (0,255,0), lineThickness)
        # for y in xrange(rows):
        #     y2 = int(minY) + gLength[1]*y
        #     cv2.line(blank_image, (int(minX), y2), (int(maxX), y2), (0,255,0), lineThickness)

        # draw datapoints (sorted)
        print(datapts)
        print("fdf")
        print(datapts[0][0][0])
        for xx in datapts:

            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(blank_image,'OpenCV',(10,500), font, 4,(255,255,255),2,cv2.LINE_AA)
            cv2.circle(blank_image, (int(xx[0][0]),int(xx[0][1])), 7, (0,100,100), -1)
            cv2.putText(blank_image,"%d" %xx[1] , (int(xx[0][0]),int(xx[0][1])) , cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1, 8)

            cv2.imshow("PAPAPAP",blank_image)
            cv2.waitKey(100)

        cv2.imshow("PAPAPAP",blank_image)
        cv2.waitKey()

        rw = rw()
        rw.writeList2File("Pin_Locations.txt",datapts)
        return Columns, Rows
