# Standard imports
from Pillow import *
import cv2
import numpy as np
import math
import time
import os
import copy
import sys
import csv

#   Function declarations
def makedir(DIR):
    if not os.path.exists(DIR):
        os.makedirs(DIR)
        time.sleep(0.5)

# Setup a path to the TSP_Data Folder
# Structure:
#   Input_Images: The folder where the input images are read (001.png is always
#                 the original-reference frame, while all others are used for
#                 processing)\
#   Datafiles   : Returns txt files with
DIR = os.path.join("TSP_Data")
directory = DIR
MovementType = os.path.join(directory, "Input_Images")

#   Option Variables
PictureFolder = MovementType
rw = rw()
# Reference pointer sets up the region of the image to look for pins
# e.g. refPt = [(originX, originY), (width, height)]
# TODO Important: as it currently stands, if a region does not adequately encapuslate
# the same number of pins between original and current picture

refPt = [(0, 55), (1000, 400)]
Rows = 9
Columns = 19
total_pins = 171

first = 1                           # first picture to be processed
last = 300                          # last picture to be processed

x1, y1 = refPt[0][0], refPt[0][1]
x2, y2 = refPt[1][0], refPt[1][1]
colour = [1]

# read from video
#cap = cv2.VideoCapture('outpy2.avi')
cap = cv2.VideoCapture('test_run_0.avi')

print(" [TSP_PinTracking_LK.py] > Video Loaded")

# Extract Reference Image
while(cap.isOpened()):
    ret, FirstImage = cap.read()

    if (FirstImage is not None):

        init    = Pillow(FirstImage, refPt)                     # a. initialize
        _       = init.cropFrame()                              # b. crop
        init.getFrameBaysian(15)                                # c. High Pass Filter
        cv2.imshow("HighPass:", init.getModifiedFrame())
        init.applyThreshold(60,False)                           # d. Threshold
        init.applyGaussianBlur(5)                               # e. Blur
        # init.rotateImage(-2.3)                                # f. Rotate ( pin grid should be vertical to the image origin )

        ROI1 = init.getModifiedFrame()

        # 2. get the pin positions as blobs
        # keypoints_orig      = init.detectorParameters().detect(ROI1)
        #
        # # 3.    Sending the keypoints data to the class Pins in Pillow.py.
        # #       Saves regions of the pins in Pin_Regions.txt
        # Columns, Rows       = Pins(refPt).main(keypoints_orig)
        #
        # # 4. Read the numbered regional data from the "Pin_Regions.txt" file -  TODO : the only reason the file is needed is in order to have 2 scripts - one for ref and one for
        # xyn                 = rw.readFile2List("Pin_Regions.txt")
        #
        # # 5. Find the coordinates of the pins in the Reference Image
        # data1               = init.getDataSet(keypoints_orig, xyn, Columns, Rows)

        # 6. In the case of the Lucas&kanade algorithm being used, extract the position of pins as features
        feature_params = dict( maxCorners = total_pins, qualityLevel = 0.3, minDistance = 7, blockSize = 7 )
        pinRef = cv2.goodFeaturesToTrack(ROI1, mask = None, **feature_params)

        blank_image = np.zeros((720,1280,3), np.uint8)
        for xx in pinRef:
            cv2.circle(blank_image,((int(xx[0][0]),int(xx[0][1])) ), 10, (0,0,200), -1)
            cv2.imshow("GoodFeatures",blank_image)

        # Get the pins as an array of values and store them in a file
        data1               = init.getDataSetLK(pinRef, True)

        cv2.imshow("Reference Image:", ROI1)
        cv2.waitKey()
        break


print(" [TSP_PinTracking_LK.py] > Data from first Image extracted")
# -----------------------------------------------------------------------------
# Extract subsequent frames #

old_frame = ROI1
lk_params = dict( winSize  = (15,15), maxLevel = 2, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

while cap.isOpened():

    # iterate over all images in folder "Input_Images"
    for iter in range(first,last):
        picture = iter

        ret, SecondImage = cap.read()
        BearingImage = np.zeros(((y2 - y1), (x2 - x1), 3), np.uint8)
        BlackImage   = copy.copy(BearingImage)

        if (SecondImage is not None):
            # ------------------------------------------------------------------
            # 1.  Process Current Image
            rec             = Pillow(SecondImage, refPt)           # a. initialize
            frame_with_box  = rec.cropFrame()                      # b. crop
            rec.getFrameBaysian(15)                                # c. High Pass Filter
            rec.applyThreshold(60,False)                             # d. Threshold
            rec.applyGaussianBlur(5)                               # e. Blur
            # rec.rotateImage(-2.3)                                  # f. Rotate ( pin grid should be vertical to the image origin )

            ROI = rec.getModifiedFrame()
            Frame = SecondImage[y1:y2, x1:x2]

            pinCurr, st, err = cv2.calcOpticalFlowPyrLK(old_frame, ROI, pinRef, None, **lk_params)

            # Select good points
            good_new = pinCurr[st==1]
            good_old = pinRef[st==1]

            # Now update the previous frame and previous points
            old_frame = ROI.copy()
            pinRef = good_new.reshape(-1,1,2)

            # Get the pins as an array of values and store them in a file
            data2               = init.getDataSetLK(pinRef)

            DistanceBearing     = rec.measurements(data1, data2, total_pins)

            ''' extract final data feature
                [pin_no, origX, origY, origSize, currX, currY, currSize, state, dx, dy, distance, bearing, changeinSize]
            '''
            DATA                = [data1[i] + data2[i][1:] + DistanceBearing[i][1:]  for i in xrange(total_pins)]
            Directory           = os.path.join(directory, "DataFiles")
            makedir(Directory)
            rw.writeList2File(os.path.join(Directory, "Data_LK_%d.txt" % picture), DATA)       # export the measurements to a file "Data_i.txt"

            with open( os.path.join(Directory, "CSV_LK_%d.csv" % picture), 'w') as csvfile:
                fieldnames = ['pin_no', 'refX', 'refY', 'refSize', 'currX', 'currY', 'currSize', 'state', 'dx', 'dy', 'distance', 'bearing', 'changeInSize' ]
                writer = csv.writer(csvfile)
                writer.writerow(fieldnames)
                writer.writerows(DATA)

            # Visualize data
            # Calculate the distance between the pins and display the distance as a change of colour.
            DATAsplit = chunker(DATA, Columns)
            [Splits.append(Splits[-1]) for Splits in DATAsplit]
            DATAsplit.append(DATAsplit[-1])
            DATAarray = np.array(DATAsplit)

            # for i in range(len(DATAarray[:]) - 1):            # Number of rows.
            #     for j in range(len(DATAarray[i][:]) - 1):     # Number of columns in each row.
            #         # Calculate the Distance between the pins
            #         d1y = math.sqrt((int(DATAarray[i + 1][j][1]) - int(DATAarray[i][j][1])) ** 2 + (int(DATAarray[i + 1][j][2]) - int(DATAarray[i][j][2])) ** 2)
            #         d1x = math.sqrt((int(DATAarray[i][j + 1][1]) - int(DATAarray[i][j][1])) ** 2 + (int(DATAarray[i][j + 1][2]) - int(DATAarray[i][j][2])) ** 2)
            #         d2y = math.sqrt((int(DATAarray[i + 1][j][4]) - int(DATAarray[i][j][4])) ** 2 + (int(DATAarray[i + 1][j][5]) - int(DATAarray[i][j][5])) ** 2)
            #         d2x = math.sqrt((int(DATAarray[i][j + 1][4]) - int(DATAarray[i][j][4])) ** 2 + (int(DATAarray[i][j + 1][5]) - int(DATAarray[i][j][5])) ** 2)
            #         colour.append((d2x-d1x)); colour.append((d2y-d1y))
            #         m = ((255 - 100) / (max(colour) - min(colour)))
            #         cv2.line(Frame, (int(DATAarray[i][j][4]), int(DATAarray[i][j][5])), (int(DATAarray[i + 1][j][4]), int(DATAarray[i + 1][j][5])), (0, m * abs(d2y - d1y) + 100, 0), 5)
            #         cv2.line(Frame, (int(DATAarray[i][j][4]), int(DATAarray[i][j][5])), (int(DATAarray[i][j + 1][4]), int(DATAarray[i][j + 1][5])), (0, m * abs(d2x - d1x) + 100, 0), 5)
            #         #cv2.rectangle(BearingImage, (int(DATAarray[i-1][j][13]), int(DATAarray[i-1][j][14])), (int(DATAarray[i][j-1][13]), int(DATAarray[i][j-1][14])), (255,255,255), -1)

            for data in DATA:
                # Drawing the bearings
                colour.append(data[12])
                yy2 = 255
                yy1 = 20
                pinSizeX2 = 3.0
                pinSizeX1 = 0.0
                pinDistX2 = 10.0
                pinDistX1 = 0.0
                mPS = ((yy2 - yy1) / (pinSizeX2 - pinSizeX1))
                mPD = ((yy2 - yy1) / (pinDistX2 - pinDistX1))
                cv2.line(BearingImage, (int(data[1]), int(data[2])), (int((data[1]) + 10 * math.sin(math.radians(data[11]))),
                						int((data[2]) + 10 * math.cos(math.radians(data[11])))), (mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1), 1)
                cv2.line(BearingImage, (int(data[1]), int(data[2])), (int((data[1]) - 200 * math.sin(math.radians(data[11]))),
                						int((data[2]) - 200 * math.cos(math.radians(data[11])))), (mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1), 1)
                cv2.circle(BearingImage, (int((data[1]) + 10 * math.sin(math.radians(data[11]))),
                						int((data[2]) + 10 * math.cos(math.radians(data[11])))), 1, (10*mPD * abs(data[10]) + yy1, 0, 0), 1)
                cv2.putText(BearingImage, "%.3f" % data[10], (int(data[1]) - 14, int(data[2]) - 14), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 10*mPD * abs(data[10]) + yy1, 0), 1)
                cv2.putText(BearingImage, "%.3f" % data[12], (int(data[1]) - 14, int(data[2]) + 14), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 10*mPS * abs(data[12]) + yy1), 1)
                # Draw on the Image
                cv2.putText(Frame, "%d" % data[0], (int(data[4]) - 7, int(data[5]) - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255), 1, 8)
                if data[7]:
                    # Draw a Line (shows the displacement of current pin in terms of its original position)
                    cv2.line(Frame, (int(data[1]), int(data[2])), (int(data[4]), int(data[5])), (0, 0, 255), 2)


                cv2.circle(ROI, (int(data[4]), int(data[5])), 10, (0, 100, 255), 2)
                cv2.circle(Frame, (int(data[4]), int(data[5])), 1, (0, 0, 255), 2)

            frame_with_box[y1:y2, x1:x2] = Frame
            # Creates a black image and sets each pixel value as white.
            width = 60
            whiteBar = np.zeros((width, int(np.shape(frame_with_box)[1]), 3), np.uint8); whiteBar.fill(255)
            # Sets the region specified to be equal to the white image create above.
            frame_with_box[0:width, 0:int(np.shape(frame_with_box)[1])] = whiteBar
            # Give the frame a title and display the number of blobs.
            cv2.putText(frame_with_box, "Video Frame %d " % iter, (5, width-15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)
            cv2.putText(frame_with_box, "Tracking %d pins" % DATA[-1][0], (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)

            # Show the frames
            Proc1 = os.path.join(directory, "Processed", "Overview")
            Proc2 = os.path.join(directory, "Processed", "Bearings")
            makedir(Proc1)
            makedir(Proc2)
            #cv2.imwrite(os.path.join(Proc1, "Overview%d.png" % picture), frame_with_box)
            #cv2.imwrite(os.path.join(Proc2, "Bearings%d.png" % picture), BearingImage)
            #cv2.imshow("Overview%d.png" % picture, frame_with_box)
            #cv2.imshow("Bearings%d.png" % picture, BearingImage)
            cv2.imshow("Overviewd.png" , frame_with_box)
            cv2.imshow("Bearings.png", BearingImage)
            # cv2.imshow("Ref.png", ROI)
            #cv2.imshow("Curr.png", ROI)


            # Check for exit statement
            k = cv2.waitKey(100) & 0xFF
            # press 'q' to exit
            if k == ord('q'):
                break
            else:
                print(" [TSP_PinTracking_LK.py] > continuing loop")

print("[TSP_PinTracking_LK.py] > Program exited successfully")
