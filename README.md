# Motion Capture Pillow

The motion capture pillow is a soft robotic sensor, shaped as a pillow that can accurately
track the exact position and orientation of a person lying on it. It is intended as a part of
a bigger robotic system that aims to replace the use of thermoplastic mask in head and
neck radiotherapy treatment. The main idea is that if minute movements of the head can
be accurately tracked, it would be easy to implement a robotic device that can reposition
the head without the need of encapsulation. Research on this area is currently being
conducted with the use of robotic treatment tables that can adjust the positioning of
patients and can correct for patient placement errors during immobilization procedures
such as those during head and neck radiotherapy.

Folder structure is the following:
• arduino-pneumatics - controller program that regulates pressure on the pillow
• open-cv-scripts - Pin tracking with OpenCv - used for testing/ validating
experimental results
• ros-workspace - listener node
• solidworks files - sensor design, and molds
• matlab - neural network scripts in matlab, make use of the output data of the
listener node
• UR_control_7.28.py - sample python script to move the robot across a set of points


Datasets from the experiments are uploaded here:
https://mega.nz/#!ZC53gIgS!rWSYIqgA6NVtdHK1nYekURLSh4h17syGzyIE1z-WdDg
