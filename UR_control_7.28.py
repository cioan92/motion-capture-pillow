# Echo client program
from numpy import *
import matplotlib.pyplot as plt
from pylab import *
import socket
import time
import re

##WARNING --- MADE FOR PYTHON 2.7

#ToDo
#Convert the IGES file values from mm to m
#the IGES file co-ordinates must be transformed to the robot global frame using numpy


def main():

	f=None
	try:
		f=open('Points.txt','r')
		#print(f.read())
		global Points
		Points= f.readlines()


	except LookupError:
		print('unknow encoding')
	except UnicodeDecodeError:
		print('decode error')
	finally:
		if f:
			f.close()


def velocity(x1,x2,y1,y2,speed):
    disp=sqrt((x2-x1)**2 + (y2-y1)**2)
    excu_time=disp/speed
    return excu_time


def URcontrol():

#define the rotation and translation matrix
    R11=1
    R12=0
    R13=0
    R21=0
    R22=1
    R23=0
    R31=0
    R32=0
    R33=1
    T1=0.25#-0.65
    T2=-0.4#0.058
    T3=0.2#0.1195
    Trans=matrix([[T1,T2,T3]],dtype=float)
    Rotat=matrix([[R11,R21,R31],[R12,R22,R32],[R13,R23,R33]],dtype= float)
#initialise viriables
    #print(Points)
    apoints=[]
    x=[]
    y=[]
    b=[]
    b=Points[0].split(',')
    listlen=(len(b)-1)/3
    print(listlen)
    #print(b)
    for m in range (listlen):
                        x.append(float(b[3*m]))
                        y.append(float(b[3*m+1]))
    plt.plot(x,y,'r')
    plt.legend(['glue path'])

    time.sleep(1)





# now start to connect with the robot

    HOST = "164.11.72.174"    # The remote host HOST = "164.11.72.215"
    PORT = 30002              # The same port as used by the server
    print ("Starting Program")

    count = 0
    target_speed=0.012

    last_x=float(b[0])+Trans[0,0]-0.004
    last_y=float(b[1])+Trans[0,1]
    x0=str(float(b[0])+Trans[0,0])
    y0=str(float(b[1])+Trans[0,1])
    z0=str(Trans[0,2])#(float(b[2])+Trans[0,2])


    while (count < 1):
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # s.setsockopt(socket.SOL_SOCKET, 25, "enp2s0")
                # s.setsockopt(socket.SOL_SOCKET, 25, str("enp2s0" + '\0').encode('utf-8'))
                s.connect((HOST, PORT))
                time.sleep(0.05)
                s.send ("set_digital_out(1,True)" + "\n")
                time.sleep(0.1)
                print ("0.2 seconds are up already")
                s.send ("set_digital_out(1,False)" + "\n")
                time.sleep(2)
#move the robot to start position
                textToSend = str("movel(p[" + x0 + "," + y0 + "," + z0 + "," + "2.1, -2.2, 0], a=0.1, v=0.1, r=0.001)"+ "\n")
                s.send (textToSend + "\n")
                time.sleep(5)
                print('ok!Already there:',x0,y0,z0)

#start loop to do the motion
                i=0
                while i<listlen:
                #for i in range (listlen):
                        c=[]
                        bpoints=[]
                        blist=[]
                        mess=[]
                        bpoints.append(float(b[3*i]))
                        bpoints.append(float(b[3*i+1]))
                        bpoints.append(float(b[3*i+2]))
                        result=(bpoints*Rotat) + Trans
                        e_time=velocity(last_x,result[0,0],last_y,result[0,1],target_speed)
                        time1=str(e_time)
                        if e_time>0.05:
                            time2=e_time-0.05
                        else:
                            time2=e_time
                        print(e_time)

                        mess.append(str(result[0,0]))
                        mess.append(str(result[0,1]))
                        mess.append(str(result[0,2]))
                        last_x=result[0,0]
                        last_y=result[0,1]
                        #textToSend = str("movel(p[" + mess[0] + "," + mess[1] + "," + mess[2] + "," + "2.1, -2.2, 0], a=0.1, v=0.05, r=0.001)"+ "\n")
                        textToSend = str("servoj(get_inverse_kin(p[" + mess[0] + "," + mess[1] + "," + z0 + "," + "2.1, -2.2, 0]), t=" + time1 + ",lookahead_time=0.03)"+ "\n")

                        s.send (textToSend + "\n")
                        print(textToSend)
                        i=i+1
                        time.sleep(time2)




                time.sleep(3)
                s.send ("set_digital_out(2,True)" + "\n")
                time.sleep(0.05)
                print ("0.2 seconds are up already")
                s.send ("set_digital_out(2, False)" + "\n")
                time.sleep(1)
                count = count + 1
    print ("The count is:", count)
    time.sleep(1)
    data = s.recv(1024)
    s.close()
    #print ("Received", repr(data))
    print ("Program finish")
    plt.show()

if __name__ == '__main__':
    main()
    URcontrol()
