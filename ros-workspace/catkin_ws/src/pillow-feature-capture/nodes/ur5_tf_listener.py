#!/usr/bin/env python
import roslib
roslib.load_manifest('pillow-feature-capture')
import rospy
import math
import os
import tf
from featureExtractor import *
#import geometry_msgs.msg

import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler

if __name__ == '__main__':
	rospy.init_node('ur5_tf_listener')

	# initialize the Pin tracking camera
	fe = featureExtractor(True, False)											# (recordFrames, windowedOutput)
	print("[ur5_tf_listener.py] > Feature extractor initialized" )
	print("[ur5_tf_listener.py] > Features are saved to folder:", os.getcwd())
	fe.getReferenceFrame()
	print("[ur5_tf_listener.py] > Data from first image extracted" )

	# Create a listener that receives the position of the end-effector
	listener = tf.TransformListener()
	# 10Hz, listener will check for manipulator changes 10 times/sec
	rate = rospy.Rate(10.0)
	print("[ur5_tf_listener.py] > Tf Listener set" )

	while not rospy.is_shutdown():
		# now = rospy.Time.now()

		try:
			#(trans,rot) = listener.lookupTransform('/ee_link', '/base_link', rospy.Time.now())
			# (trans,rot) = listener.lookupTransform('/ee_link', '/base_link', rospy.Time(0))
			# (trans,rot) = listener.lookupTransform( '/base','/tool0_controller', rospy.Time(0))
			(trans,rot) = listener.lookupTransform('/base','/tool0_controller', rospy.Time(0))

		except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as ex:
			template = "An exception of type {0} occurred. Arguments:\n{1!r}"
			message = template.format(type(ex).__name__, ex.args)
			print message
			continue

		eulerA = euler_from_quaternion ([rot[0], rot[1], rot[2], rot[3]])

		# Get EE coordinates [x,y,z,qx,qy,qz,qw]
		EECoords = [1000*trans[0], 1000*trans[1], 1000*trans[2], eulerA[0], eulerA[1], eulerA[2], 0]


		# Print the published transformation to the command line
		print(" Transform [x,y,z]: [%.4f,%.4f, %.4f] \n Rotation [qx,qy,qz,qw]: [%.4f,%.4f, %.4f, %.4f] " % (1000*trans[0], 1000*trans[1], 1000*trans[2], rot[0], rot[1], rot[2], rot[3]))
		print(" Euler_from_quaternions", euler_from_quaternion ([rot[0], rot[1], rot[2], rot[3]]))

		# invoke openCV
		dataPt = fe.getCurrentFrame()
		if dataPt is not None:
			# fe.writeDatasetToTxtFile([['robot_position ',EECoords], dataPt])
			fe.writeDatasetToCsvFile(EECoords, dataPt)
		else:
			print "Corrupt dataset"
			# fe.writeDatasetToCsvFile(EECoords, [-1 -1 -1 -1 -1 -1 ])
			# fe.writeDatasetToTxtFile([['robot_position ',EECoords], [-1 -1 -1 -1 -1 -1 ]])

		# get the feature map from the image
		fe.pinTrackerWindow()
		# save the feature map
		# save_data = true
		rate.sleep()
