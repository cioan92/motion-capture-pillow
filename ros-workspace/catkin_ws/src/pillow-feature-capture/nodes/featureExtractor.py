import cv2
from operator import itemgetter
import math
import itertools
import numpy as np
import math
import time
import os
import copy
import sys
from Pillow import *
import csv

class featureExtractor:

    def makedir(self, DIR):
        if not os.path.exists(DIR):
            os.makedirs(DIR)
            time.sleep(0.5)

    def __init__(self, recordFrames_, windowedOutput_ ):

        self.recordFrames = recordFrames_
        self.windowedOutput = windowedOutput_
        # Setup a path to the TSP_Data Folder
        # Structure:
        #   Input_Images: The folder where the input images are read (001.png is always
        #                 the original-reference frame, while all others are used for
        #                 processing)\
        #   Datafiles   : Returns txt files with the current frame
        self.DIR = os.path.join("TSP_Data")
        self.baseDir = self.DIR
        self.MovementType = os.path.join(self.baseDir, "Input_Images")

        #   Option Variables
        self.PictureFolder = self.MovementType
        self.rw = rw()

        # Reference pointer sets up the region of the image to look for pins
        # e.g. refPt = [(originX, originY), (width, height)]
        self.refPt = [(0, 55), (1000, 400)]
        self.x1, self.y1 = self.refPt[0][0], self.refPt[0][1]
        self.x2, self.y2 = self.refPt[1][0], self.refPt[1][1]
        self.colour = [1]

        # Define pin grid
        self.Rows = 9
        self.Columns = 19
        self.total_pins = 171

        # read from camera
        self.cap = cv2.VideoCapture(1)

        # frame number iterator
        self.iter = 0

        # create feature directory Folder
        self.Directory = os.path.join(self.baseDir, "DataFiles")
        self.makedir(self.Directory)

        # capture Video (onDemand)
        if (recordFrames_) :
            # Default resolutions of the frame are obtained.The default resolutions are system dependent.
            # We convert the resolutions from float to integer.
            frame_width = int(self.cap.get(3))
            frame_height = int(self.cap.get(4))

            for i in xrange(100):
                filename_ = "./test_run_" + str(i) + ".avi"
                if (not os.path.isfile(filename_)):
                    # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
                    self.out = cv2.VideoWriter(filename_, cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width, frame_height))
                    break

    def getReferenceFrame(self):

        # Extract Reference Image
        while(self.cap.isOpened()):
            ret, FirstImage = self.cap.read()

            if (FirstImage is not None):

                self.init    = Pillow(FirstImage, self.refPt)                     # a. initialize
                _       = self.init.cropFrame()                              # b. crop
                self.init.getFrameBaysian(15)                                # c. High Pass Filter
                self.init.applyThreshold(60,False)                           # d. Threshold
                self.init.applyGaussianBlur(5)                               # e. Blur
                # self.init.rotateImage(-2.3)                                # f. Rotate ( pin grid should be vertical to the image origin )

                ROI1 = self.init.getModifiedFrame()

                # 6. In the case of the Lucas&kanade algorithm being used, extract the position of pins as features
                feature_params = dict( maxCorners = self.total_pins, qualityLevel = 0.3, minDistance = 7, blockSize = 7 )
                self.pinRef = cv2.goodFeaturesToTrack(ROI1, mask = None, **feature_params)

                blank_image = np.zeros((480,640,3), np.uint8)
                for xx in self.pinRef:
                    cv2.circle(blank_image,((int(xx[0][0]),int(xx[0][1])) ), 5, (0,0,150), -1)
                    cv2.imshow("Feature Map",blank_image)

                # Get the pins as an array of values and store them in a file
                self.data1               = self.init.getDataSetLK(self.pinRef, True)

                if (self.recordFrames) :
                    self.out.write(FirstImage)

                cv2.imshow(" Reference Original", FirstImage)
                cv2.imshow(" Reference Image", ROI1)
                cv2.waitKey()
                break


        self.old_frame = ROI1
        self.lk_params = dict( winSize  = (15,15), maxLevel = 2, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

        print(" [featureExtractor.py] > Data from first Image extracted")

    def getCurrentFrame(self):

        if self.cap.isOpened():

            self.picture = self.iter
            self.iter = self.iter + 1

            ret, self.SecondImage = self.cap.read()
            self.cpySecondImage = copy.copy(self.SecondImage)
            self.BearingImage = np.zeros(((self.y2 - self.y1), (self.x2 - self.x1), 3), np.uint8)
            self.BlackImage   = copy.copy(self.BearingImage)

            if (self.SecondImage is not None):
                # ------------------------------------------------------------------
                # 1.  Process Current Image
                rec             = Pillow(self.SecondImage, self.refPt)              # a. initialize
                self.frame_with_box  = rec.cropFrame()                              # b. crop
                rec.getFrameBaysian(15)                                # c. High Pass Filter
                rec.applyThreshold(60,False)                           # d. Threshold
                rec.applyGaussianBlur(5)                               # e. Blur
                # rec.rotateImage(-2.3)                                # f. Rotate ( pin grid should be vertical to the image origin )

                self.ROI = rec.getModifiedFrame()
                self.Frame = self.SecondImage[self.y1:self.y2, self.x1:self.x2]

                pinCurr, st, err = cv2.calcOpticalFlowPyrLK(self.old_frame, self.ROI, self.pinRef, None, **self.lk_params)

                # Select good points
                self.good_new = pinCurr[st==1]
                self.good_old = self.pinRef[st==1]

                # Now update the previous frame and previous points
                self.old_frame = self.ROI.copy()
                self.pinRef = self.good_new.reshape(-1,1,2)

                # Get the pins as an array of values and store them in a file
                self.data2               = self.init.getDataSetLK(self.pinRef)
                self.DistanceBearing     = rec.measurements(self.data1, self.data2, self.total_pins)

                ''' extract final data feature
                    [pin_no, origX, origY, origSize, currX, currY, currSize, bearing,regx, regiony]
                '''
                self.DATA                = [self.data1[i] + self.data2[i][1:] + self.DistanceBearing[i][1:]  for i in xrange(self.total_pins)]
                return self.DATA

            else:
                print(" [featureExtractor.py] > ERROR, Camera Stream not open")
                return None

    def writeDatasetToTxtFile(self, dtPts):
        self.rw.writeList2File(os.path.join(self.Directory, "Data_LK_%d.txt" % self.picture), dtPts)       # export the measurements to a file "Data_i.txt"
        print(self.Directory)

    def writeDatasetToCsvFile(self,rbPts,dtPts):
        print self.Directory
        with open( os.path.join(self.Directory, "CSV_LK_%d.csv" % self.picture), 'w') as csvfile:
            writer = csv.writer(csvfile)
            fieldnames = ['x', 'y', 'z', 'rotY', 'rotX', 'rotZ', 'qw' ]
            writer.writerow(fieldnames)
            writer.writerow(rbPts)
            fieldnames = ['pin_no', 'refX', 'refY', 'refSize', 'currX', 'currY', 'currSize', 'state', 'dx', 'dy', 'distance', 'bearing', 'changeInSize' ]
            writer.writerow(fieldnames)
            writer.writerows(dtPts)

    def pinTrackerWindow(self):

        if self.windowedOutput:
            # Visualize data
            # Calculate the distance between the pins and display the distance as a change of colour.
            DATAsplit = chunker(self.DATA, self.Columns)
            [Splits.append(Splits[-1]) for Splits in DATAsplit]
            DATAsplit.append(DATAsplit[-1])
            DATAarray = np.array(DATAsplit)

            for data in self.DATA:
                # Drawing the bearings
                self.colour.append(data[12])
                yy2 = 255
                yy1 = 20
                pinSizeX2 = 3.0
                pinSizeX1 = 0.0
                pinDistX2 = 10.0
                pinDistX1 = 0.0
                mPS = ((yy2 - yy1) / (pinSizeX2 - pinSizeX1))
                mPD = ((yy2 - yy1) / (pinDistX2 - pinDistX1))
                cv2.line(self.BearingImage, (int(data[1]), int(data[2])), (int((data[1]) + 10 * math.sin(math.radians(data[11]))),
                						int((data[2]) + 10 * math.cos(math.radians(data[11])))), (mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1), 1)
                cv2.line(self.BearingImage, (int(data[1]), int(data[2])), (int((data[1]) - 200 * math.sin(math.radians(data[11]))),
                						int((data[2]) - 200 * math.cos(math.radians(data[11])))), (mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1, mPD * abs(data[10]) + yy1), 1)
                cv2.circle(self.BearingImage, (int((data[1]) + 10 * math.sin(math.radians(data[11]))),
                						int((data[2]) + 10 * math.cos(math.radians(data[11])))), 1, (10*mPD * abs(data[10]) + yy1, 0, 0), 1)
                cv2.putText(self.BearingImage, "%.3f" % data[10], (int(data[1]) - 14, int(data[2]) - 14), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 10*mPD * abs(data[10]) + yy1, 0), 1)
                cv2.putText(self.BearingImage, "%.3f" % data[12], (int(data[1]) - 14, int(data[2]) + 14), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 10*mPS * abs(data[12]) + yy1), 1)
                # Draw on the Image
                cv2.putText(self.Frame, "%d" % data[0], (int(data[4]) - 7, int(data[5]) - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255), 1, 8)
                if data[7]:
                    # Draw a Line (shows the displacement of current pin in terms of its original position)
                    cv2.line(self.Frame, (int(data[1]), int(data[2])), (int(data[4]), int(data[5])), (0, 0, 255), 2)

                cv2.circle(self.ROI, (int(data[4]), int(data[5])), 10, (0, 100, 255), 2)
                cv2.circle(self.Frame, (int(data[4]), int(data[5])), 1, (0, 0, 255), 2)

            self.frame_with_box[self.y1:self.y2, self.x1:self.x2] = self.Frame
            # Creates a black image and sets each pixel value as white.
            width = 60
            whiteBar = np.zeros((width, int(np.shape(self.frame_with_box)[1]), 3), np.uint8); whiteBar.fill(255)
            # Sets the region specified to be equal to the white image create above.
            self.frame_with_box[0:width, 0:int(np.shape(self.frame_with_box)[1])] = whiteBar
            # Give the frame a title and display the number of blobs.
            cv2.putText(self.frame_with_box, "Video Frame %d " % self.iter, (5, width-15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)
            cv2.putText(self.frame_with_box, "Tracking %d pins" % self.DATA[-1][0], (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)

            # Show the frames
            Proc1 = os.path.join(self.baseDir, "Processed", "Overview")
            Proc2 = os.path.join(self.baseDir, "Processed", "Bearings")
            self.makedir(Proc1)
            self.makedir(Proc2)

            cv2.imshow("Overviewd.png" , self.frame_with_box)
            cv2.imshow("Bearings.png", self.BearingImage)
            cv2.waitKey(1)

        # # save the video file
        if self.recordFrames:
            self.out.write(self.cpySecondImage)
