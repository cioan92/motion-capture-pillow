/*
 Name:		mcp_low_pressure.ino
 Created:	6/11/2019 1:18:23 PM
 Author:	Christos
*/

#include "serial.h"

#define FORWARD_DRIVE 1
#define BACKWARD_DRIVE 2
#define STOP 3

#define A1A  2			//define pin 2 for A1A (Air Pump)
#define A1B  3			//define pin 3 for A1B (Air Pump)
#define B1A  8			//define pin 8 for B1A (Solenoid)
#define B1B  9			//define pin 9 for B1B (Solenoid)
#define PRSNSR_PIN A0	// Analog input pin that the potentiometer is attached to
#define LOOP_DELAY 100

#define convert_to_psi(measurement) (( 675.17 - measurement)/55.809)
#define PSI_DIFF 0.08

int sensorValue = 0;	// value read from the pressure sensor via the amplifier stage
float measured_pressure = 0;	// value output to the Serial port and LCD display
int iterator = 0;
int incomingByte = 0;		
int pump_state = FORWARD_DRIVE;	// state of the air pump
int sol_state = FORWARD_DRIVE;	// state of the solenoid


// the setup function runs once when you press reset or power the board
void setup() {
	
	pinMode(B1A, OUTPUT);
	pinMode(B1B, OUTPUT);
	pinMode(A1A, OUTPUT);
	pinMode(A1B, OUTPUT);

	delay(LOOP_DELAY);
	Serial.begin(9600);
	Serial.println("Setup_complete");
}

// the loop function runs over and over again until power down or reset
void loop() {

  // read serial input
	receive_serial();
	get_state_change();

  maintain_pressure(0.90);

	pump_air(pump_state);
	release_sol(sol_state);

  read_pressure();

	//iterator++;
	//if (iterator >= 100) {
    //Serial.println("Solenoid_is_set");
	//}

	delay(LOOP_DELAY);
}

/* 
	void pump_air(bool state)
	Actuates the air pump 
*/
void pump_air(int state) {

	switch (state) {

		case FORWARD_DRIVE:{
			digitalWrite(A1A, LOW);
			digitalWrite(A1B, HIGH);
			break;
		}
		case BACKWARD_DRIVE:{
			digitalWrite(A1A, HIGH);
			digitalWrite(A1B, LOW);
			break;
		}
		case STOP:{
			digitalWrite(A1A, LOW);
			digitalWrite(A1B, LOW);
			break;
		}
	}
}

void release_sol(int state) {

	switch (state) {

		case FORWARD_DRIVE: {
			digitalWrite(B1A, LOW);
			digitalWrite(B1B, HIGH);
			break;
		}
		case BACKWARD_DRIVE: {
			digitalWrite(B1A, HIGH);
			digitalWrite(B1B, LOW);
			break;
		}
		case STOP: {
			digitalWrite(B1A, LOW);
			digitalWrite(B1B, LOW);
			break;
		}
	}
}

void read_pressure() {

	sensorValue = analogRead(PRSNSR_PIN);
	measured_pressure = convert_to_psi(sensorValue);

	// print the results to the serial monitor:
	Serial.print("Sensor = ");
	Serial.print(sensorValue);
	Serial.print("\toutput = ");
	Serial.println(measured_pressure);

	// wait 500 milliseconds before the next loop
	// for the analog-to-digital converter to settle
	// after the last reading:
	delay(LOOP_DELAY);
}

void get_state_change() {

	if (compare_serial("qm")) {
		Serial.println("Stopping_motors");
		pump_state = STOP;
	}
	if (compare_serial("qs")) {
		Serial.println("Stopping_solenoid");
		sol_state = STOP;
	}	
	if (compare_serial("sm")) {
		Serial.println("Start_motors (Forward)");
		pump_state = FORWARD_DRIVE;
	}	
	if (compare_serial("ss")) {
		Serial.println("Start_solenoid (Forward)");
		sol_state = FORWARD_DRIVE;
	}
	if (compare_serial("Sm")) {
		Serial.println("Start_motors (Backward)");
		pump_state = BACKWARD_DRIVE;
	}
	if (compare_serial("Ss")) {
		Serial.println("Start_solenoid (Backward)");
		sol_state = BACKWARD_DRIVE;
	}
 flush_serial();
}

void maintain_pressure(float psi_val){

  float p_diff = measured_pressure - psi_val;

  // Stop motors at the desired pressure
  if ( p_diff > 0 ){
    // stop pumping
    if (p_diff < PSI_DIFF){
        Serial.println("Stopping_motors");
        pump_state = STOP;
        Serial.println("Start_solenoid (Forward)");
        sol_state = FORWARD_DRIVE;
    }
    else{
        Serial.println("Stopping_solenoid");
        sol_state = STOP;
    }
  }
  else{
    if (p_diff < - PSI_DIFF){
      /*Do nothing*/
    }
    else{
      Serial.println("Start_motors (Forward)");
      pump_state = FORWARD_DRIVE;
      Serial.println("Start_solenoid (Forward)");
      sol_state = FORWARD_DRIVE;  
    }
  }

/*
  if (measured_pressure > psi_val ){
    Serial.println("Stopping_solenoid");
    sol_state = STOP;
  }
  else {
    Serial.println("Start_solenoid (Forward)");
    sol_state = FORWARD_DRIVE;
  }
*/
}
