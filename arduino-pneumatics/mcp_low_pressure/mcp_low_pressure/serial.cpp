// 
// 
// 

#include "serial.h"

char receivedChars[32];   // an array to store the received data
bool newData = false;

void receive_serial() {
	static byte ndx = 0;
	char endMarker = '\n';
	char rc;

	while (Serial.available() > 0 && newData == false) {
		rc = Serial.read();

		if (rc != endMarker) {
			receivedChars[ndx] = rc;
			ndx++;
			if (ndx >= 32) {
				ndx = 32 - 1;
			}
		}
		else {
			receivedChars[ndx] = '\0'; // terminate the string
			ndx = 0;
			newData = true;
		}
	}
}

bool compare_serial(char *msg) {

  bool rtn_st = false;
	
	if (newData == true) {

		if (strcmp(receivedChars, msg) == 0) {
			//Serial.println("Stop");
      rtn_st = true;
		}

    //Serial.print("Received:  ");
   // Serial.println(receivedChars);
    
	}
  return(rtn_st);
}

void flush_serial(){
  newData = false;
}
