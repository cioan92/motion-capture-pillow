// serial.h

#ifndef _SERIAL_h
#define _SERIAL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

extern char receivedChars[32];   // an array to store the received data
extern bool newData;

void receive_serial();
bool compare_serial(char *msg);
void flush_serial();


#endif
